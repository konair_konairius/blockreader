package blockreader

import (
	"math"
	"testing"
)

const (
	testfilepath  = "/home/konsti/Downloads/ubuntu-14.10-server-amd64.iso"
	testblocksize = 1048576 //1MB
)

//Helper Functions

func getReader(t *testing.T) Reader {
	r, err := New(testfilepath, testblocksize)
	if err != nil {
		t.Errorf("New Failed: %v", err)
	}
	return r
}

//Real tests

func Test_NewReader(t *testing.T) {
	getReader(t)
}

func Test_Reader_BlockSize(t *testing.T) {
	r := getReader(t)
	if r.BlockSize() != testblocksize {
		t.Fatal("BlockSize() did not return expected Value")
	}
}

func Test_Reader_Blockchannel(t *testing.T) {
	r := getReader(t)
	c := r.BlockChannel()
	o := 0
	for b := range c {
		t.Logf("Got Block at: %dMB", (o / int(math.Pow(2, 20))))
		o += len(b)
	}
}

func Test_Reader_Test_Consitency(t *testing.T) {
	r1 := getReader(t)
	r2 := getReader(t)
	c1 := r1.BlockChannel()
	c2 := r2.BlockChannel()

	d1 := <-c1
	d2 := <-c2
	for _, i := range d1 {
		if d1[i] != d2[i] {
			t.Errorf("%2.0x-%2.0x", d1[i], d2[i])
		}
	}
}
