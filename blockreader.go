//This Packeage Provides a simple implementation of a Reader that uses channels to read a file
package blockreader

import (
	"fmt"
	"io"
	"os"
)

//Interface for accessing the readers
type Reader interface {
	//Returns the maximum block Size
	BlockSize() uint
	//Returns a readonly channel that contains byte slices of maximum size BlockSize()
	//The Channel is closed when for examle we reach EOF on a FSReader
	BlockChannel() <-chan []byte
}

//Describes a Reader for a Filesystem
type FSReader struct {
	path         string
	blocksize    uint
	blockchannel chan []byte
}

//Used to create a FSReader
func New(path string, blocksize uint) (FSReader, error) {

	//fmt.Printf("Path: %s, blocksize: %d\n", path, blocksize)

	c := make(chan []byte)

	f, err := os.Open(path)
	if err != nil {
		return FSReader{}, err
	}

	var worker = func() {

		defer close(c)
		defer f.Close()
		var offset int64 = 0

		for {
			readBuffer := make([]byte, blocksize) //f.Read does strange stuff if the buffer it gets is not "fresh"
			// fmt.Printf("Offset: %d\n", offset)
			n, err := f.ReadAt(readBuffer, offset)
			if err != nil && err != io.EOF {
				fmt.Errorf("%v", err)
				break
			}
			// fmt.Printf("Read: %d\n", n)
			if n == 0 {
				//fmt.Printf("Nothing to do!")
				break
			}
			if n > int(blocksize) {
				fmt.Printf("This should not happen!")
				break
			}
			offset += int64(n)
			c <- readBuffer[:n]
		}
	}

	go worker()

	r := FSReader{
		blocksize:    blocksize,
		path:         path,
		blockchannel: c,
	}

	return r, nil
}

//Returns the maximum block Size
func (r FSReader) BlockSize() uint {
	return r.blocksize
}

//Returns a readonly channel that contains byte slices of maximum size BlockSize()
//The Channel is closed when we reach EOF.
func (r FSReader) BlockChannel() <-chan []byte {
	return r.blockchannel
}
